# TC-300 MKS-50 programmer #

*A Teensy based MIDI controller for the Roland MKS-50/Alpha Juno*

Live demo: https://youtu.be/3QjlO-yeFrc  
Demo song: https://youtu.be/PmCOnhFrTqU


[![Overview](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/thumbnails/2016-02-27%2012.20.46-2.jpg)](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/high-resolution/2016-02-27%2012.20.46-2.jpg)
[![Panel with controls](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/thumbnails/2016-02-15%2018.47.25-2.jpg)](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/high-resolution/2016-02-15%2018.47.25-2.jpg)
[![Wiring](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/thumbnails/2016-02-26%2009.58.11.jpg)](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/high-resolution/2016-02-26%2009.58.11.jpg)
[![Wiring](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/thumbnails/2016-02-26%2009.59.09.jpg)](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/high-resolution/2016-02-26%2009.59.09.jpg)
[![Wiring](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/thumbnails/2016-02-17%2021.33.07.jpg)](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/high-resolution/2016-02-17%2021.33.07.jpg)
[![Wiring](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/thumbnails/2016-02-17%2021.36.17-2.jpg)](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/high-resolution/2016-02-17%2021.36.17-2.jpg)
[![Panel print closeup](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/thumbnails/2016-02-13%2011.24.14.jpg)](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/high-resolution/2016-02-13%2011.24.14.jpg)
[![Panel print closeup](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/thumbnails/2016-02-13%2011.24.48.jpg)](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/high-resolution/2016-02-13%2011.24.48.jpg)
[![Panel print closeup](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/thumbnails/2016-02-13%2011.25.25.jpg)](https://bytebucket.org/loveaurell/roland-alpha-juno-mks-50-midi-controller/raw/master/images/high-resolution/2016-02-13%2011.25.25.jpg)

Background
----------

The Roland MKS-50 has very limited controlling abilities out of the box and depends on having an external programmer like the PG-300 to use it beyond its presets. Since it's quite old, produced in 1987, it uses its own sysex implementation instead of ordinary cc messages for most of it's parameters. That makes it hard to use a geneneral purpose MIDI device for controlling it - either it does not support customized sysex messages or it just doen't have enough controls to make all the synthesizers parameters available at once.

The controls
------------

The TC-300 has 46 knobs and switches that not only makes the user in control of most of the useful parameters of the synthesizer, but also adds the possibility to assign the mod wheel and control pedal to any of the rotary controls on the panel. It even has a button for dumping the current state of all controls.

Accessible parameters and functions
-----------------------------------

#### DCO ####
* Transpose (-12 to +12)
* Octave (4', 8', 16', 32')
* Finetune

* Pulse wave (3 waveforms)
* Saw wave (5 waveforms)
* Sub wave (5 waveforms)
* PWM rate & width (a LED is indicating if a pulse or saw waveform that supports PWM is chosen).
* Sub level (4 steps)
* Noise level (4 steps)

* Envelope mode
* Envelope modulation amount
* Aftertouch modulation amount
* LFO modulation amount

#### LFO ####
* Rate
* Delay

#### VCA ####
* Level
* "Hold" function that keeps sustain active even after releasing the physical pedal. 
* Envelope mode
* Aftertouch modulation amount

#### VCF ####
* Cutoff
* Resonance
* Highpass (4 steps)
* Key follow amount

* Envelope mode
* Envelope modulation amount
* Aftertouch modulation amount
* LFO modulation amount

#### Envelope ####
* 7 parameters/stages
* Key follow amount

#### GLIDE ####
* Time
* ON/OFF

#### CHORUS ####
* Rate/depth
* ON/OFF

#### MASTER ####
* Volume
* Bend range (1-12)
* Poly/mono switch
* Send control state button (Dumps all the controls physical positions to the MKS-50)

#### MOD ASSIGN ####
Two buttons for assigning the mod wheel or control pedal to any of the potentiometers/rotary switch on the controller. This is not supported on the MKS-50 itself. It's fun to play around with, especially when assigning it to the octave switch parameter :)

Enclosure
---------
When designing the panel, I took inspiration from the Roland Juno synths. The colour coding of the knobs indicate what function they have (LFO & envelope). Together with the distinct sections and the large envelope sketch, I think I managed to make the interface very intuitive.

The design is available as an .ai-file containing both the artwork and the drilling points of the top panel. Also included is an .ai-file containing the hole specifications for the MIDI and USB jacks of the rear panel.

The panel design was sent to Takachi Enclosure for inkjet printing and punch pressing on one their aluminum cases.

Other info
----------
Here's some info about the MKS-50 and a sound demo (not made by me):  
http://www.vintagesynth.com/roland/mks50.php  
https://www.youtube.com/watch?v=uiCgFoZIH7w