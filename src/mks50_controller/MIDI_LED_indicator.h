#ifndef MIDI_LED_indicator_h
#define MIDI_LED_indicator_h

#include "Arduino.h"

class MidiLedIndicator
{
  public:
    MidiLedIndicator(int pin);
    void noteOn();
    void noteOff();
    void cc();
    void update();

    void activateflashingMode(int intervalInMs);
    void deactivateFlashingMode();
    void toggleFlashingMode();
  private:
    int _ledPin;
    bool _ledActive;
    int _numberOfActiveNotes;
    int _lastCcTimestamp;

    bool _isFlashingActive;
    unsigned int _flashingInterval;
    unsigned int _halfFlashingInterval;

    bool _ledShouldBeActive();
};

#endif
