#ifndef PWM_LED_indicator_h
#define PWM_LED_indicator_h

#include "Arduino.h"
#include "mks50_defines.h"

class PwmLedIndicator
{
  public:
    PwmLedIndicator(int pin);
    void passValue(int paramGroup, int param, int value);
    void update();
  private:
    int _ledPin;
    bool _dcoPulse3Active;
    bool _dcoSaw3Active;
    bool _pwmIndicatorLedIsLit;
};

#endif
