#ifndef lfo_h
#define lfo_h

#include "Arduino.h"

class Lfo
{
  public:
    Lfo(unsigned int samplerate);
    void update();
    bool isActive();

    void setMaxValue(unsigned int value);
    void setMinValue(unsigned int value);
    
    int getUpdatedValue();
    void setFrequencyIndex(unsigned int index);
  private:
    unsigned int _samplerate = 0;
  
    unsigned int _maxValue = 0;
    unsigned int _minValue = 0;
    unsigned int _valueRange = 0;
    
    int _updatedValue = -1;

    double _phaseAcc = 0;
    double _phaseInc = 0;

    void _updateValueRange();
    void _increment();
    unsigned int _getCalculatedValue();
    double _getMultiplier();
};

#endif



