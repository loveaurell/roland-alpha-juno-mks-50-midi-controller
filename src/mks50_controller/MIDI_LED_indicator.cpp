#include "Arduino.h"
#include "MIDI_LED_indicator.h"

MidiLedIndicator::MidiLedIndicator(int pin) {
  pinMode(pin, OUTPUT);
  
  _ledPin = pin;
  _ledActive = false;
  _numberOfActiveNotes = 0;
  _lastCcTimestamp = 0;

  _isFlashingActive = false;
  _flashingInterval = 500;
  _halfFlashingInterval = _flashingInterval / 2;
}

void MidiLedIndicator::noteOn() {
  _numberOfActiveNotes++;
}

void MidiLedIndicator::noteOff() {
  _numberOfActiveNotes--;
  if (_numberOfActiveNotes < 0) {
    _numberOfActiveNotes = 0;
  }
}

void MidiLedIndicator::cc() {
  _lastCcTimestamp = millis();
}

void MidiLedIndicator::update() {
  bool ledShouldBeActive = _ledShouldBeActive();
  if (_ledActive != ledShouldBeActive) {
    digitalWrite(_ledPin, ledShouldBeActive);
    _ledActive = ledShouldBeActive;
  }
}

bool MidiLedIndicator::_ledShouldBeActive() {
  if (_isFlashingActive) {
    return (millis() % _flashingInterval) <= _halfFlashingInterval;
  } else {
    return (_numberOfActiveNotes > 0 || (millis() - _lastCcTimestamp < 100));
  }
}

void MidiLedIndicator::activateflashingMode(int intervalInMs) {
  _isFlashingActive = true;
  _flashingInterval = intervalInMs;
  _halfFlashingInterval = intervalInMs / 2;
}

void MidiLedIndicator::deactivateFlashingMode() {
  _isFlashingActive = false;
}

void MidiLedIndicator::toggleFlashingMode() {
  _isFlashingActive = !_isFlashingActive;
}
