#include "lfo.h"

Lfo::Lfo(unsigned int samplerate) {
  _samplerate = samplerate;
  
  _maxValue = 0;
  _minValue = 0;
  _valueRange = 0;
  
  _updatedValue = -1;
  
  _phaseAcc = 0;
  _phaseInc = 0;
  _phaseInc = 0.05;
}

void Lfo::setMaxValue(unsigned int value) {
  _maxValue = value;
  _updateValueRange();
}

void Lfo::setMinValue(unsigned int value) {
  _minValue = value;
  _updateValueRange();
}

int Lfo::getUpdatedValue() {
  int value = _updatedValue;
  _updatedValue = -1;
  
  return value;
}

void Lfo::setFrequencyIndex(unsigned int index) {
  _phaseInc = (index * 0.01) / _samplerate;
}

void Lfo::update() {
  _increment();
  unsigned int value = _getCalculatedValue();
  Serial.println(value);
  _updatedValue = value;
}

bool Lfo::isActive() {
  return _valueRange > 0;
}

void Lfo::_updateValueRange(){
  _valueRange = (_maxValue > _minValue) ? (_maxValue - _minValue) : 0;
}

void Lfo::_increment() {
  _phaseAcc += _phaseInc;
  if (_phaseAcc >= 1) {
    _phaseAcc -= 1;
  }
}

unsigned int Lfo::_getCalculatedValue(){
  if (_valueRange == 0) {
    return _minValue;
  }
  
  return _minValue + _getMultiplier() * _valueRange;
}

double Lfo::_getMultiplier() {
  if (_phaseAcc < 0.5) {
    return _phaseAcc * 2.0;
  } else {
    return 2.0 - _phaseAcc * 2.0;
  }
}
