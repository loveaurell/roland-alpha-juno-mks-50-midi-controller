#include "PWM_LED_indicator.h"

PwmLedIndicator::PwmLedIndicator(int pin) {
  pinMode(pin, OUTPUT);

  _ledPin = pin;
  
  _dcoPulse3Active = false;
  _dcoSaw3Active = false;
  _pwmIndicatorLedIsLit = false;
  
}

void PwmLedIndicator::passValue(int paramGroup, int param, int value) {
  if(paramGroup == PARAM_GROUP_TONE) {
    if(param == TONE_PARAM_DCO_WAVEFORM_PULSE) {
      _dcoPulse3Active = (value == DCO_PULSE_WAVEFORM_3);
    } else if (param == TONE_PARAM_DCO_WAVEFORM_SAW) {
      _dcoSaw3Active = (value == DCO_SAW_WAVEFORM_3);
    }
  }
}

void PwmLedIndicator::update() {
  if (!_pwmIndicatorLedIsLit && (_dcoPulse3Active || _dcoSaw3Active)) {
    digitalWrite(_ledPin, HIGH);
    _pwmIndicatorLedIsLit = true;
  } else if (_pwmIndicatorLedIsLit && !_dcoPulse3Active && !_dcoSaw3Active) {
    digitalWrite(_ledPin, LOW);
    _pwmIndicatorLedIsLit = false;
  }
}
