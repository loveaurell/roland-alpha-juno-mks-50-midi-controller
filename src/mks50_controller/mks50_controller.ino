#include <MIDI.h> //Using version 4.3 of MIDI library
#include <Bounce.h>
#include <Metro.h>
#include "mks50_defines.h"
#include "MIDI_LED_indicator.h"
#include "PWM_LED_indicator.h"
#include "lfo.h"

#define LFO_SAMPLERATE 100

const int readResolution = DEFAULT_ANALOG_READ_RESOLUTION;
const int maxAnalogReadValue = (2 << (readResolution-1)) - 1;
const int jitterThreshold = (maxAnalogReadValue+1)/((MKS50_CONTROL_MAX_VALUE+1) << 1);

int midiChannel = DEFAULT_MIDI_CHANNEL;

MidiLedIndicator midiLedIndicator = MidiLedIndicator(MIDI_INDICATOR_LED);
PwmLedIndicator pwmLedIndicator = PwmLedIndicator(PWM_ACTIVE_INDICATOR_LED);

Lfo lfo = Lfo(LFO_SAMPLERATE);
Metro lfoMetro = Metro(1000 / LFO_SAMPLERATE);
boolean lfoConfigModeActive = false;
int lfoDestinationParam = -1;
int lfoDestinationParamGroup = -1;

//Controls that represent MIDI parameters on the MKS-50 synth
ControlPin controlPins[NUMBER_OF_CONTROLS];

//Variables used for sending the state of all controls to the MIDI OUT
Bounce controlDumpButton = Bounce(DIGITAL_INPUT_PIN_DUMP_CONTROLS_BUTTON, BUTTON_DEBOUNCE_DELAY);
bool forceSendControlSysex = false;

//Variables used for simulating VCA hold
Bounce vcaHoldButton = Bounce(DIGITAL_INPUT_PIN_VCA_HOLD, BUTTON_DEBOUNCE_DELAY);
bool vcaHoldActive = false;
bool sendSustainMessage = false;

//Variables used to assign modulation wheel to any variable parameter
Bounce modWheelAssignButton =   Bounce(DIGITAL_INPUT_PIN_MOD_WHEEL_ASSIGN_BUTTON, BUTTON_DEBOUNCE_DELAY);
bool modWheelAssignActive = false;
int modWheelParam = -1;
int modWheelParamGroup = -1;

//Variables used to assign control pedal to any variable parameter
Bounce modPedalAssignButton =   Bounce(DIGITAL_INPUT_PIN_MOD_PEDAL_ASSIGN_BUTTON, BUTTON_DEBOUNCE_DELAY);
bool modPedalAssignActive = false;
int modPedalParam = -1;
int modPedalParamGroup = -1;

MIDI_CREATE_DEFAULT_INSTANCE();

void handleMidiNoteOn(byte channel, byte note, byte velocity) {
  //Serial.println("Note on");
  midiLedIndicator.noteOn();
}

void handleMidiNoteOff(byte channel, byte note, byte velocity) {
  //Serial.println("Note off");
  midiLedIndicator.noteOff();
}

void handleMidiControlChange(byte channel, byte number, byte value) {
  midiLedIndicator.cc();
  //If a parameter is assigned to mod wheel, send sysex to change that value
  if (MOD_WHEEL_PARAM == number && modWheelParam > -1) {
    createAndSendSysexMessage(modWheelParamGroup, modWheelParam, value);
    //If a parameter is assigned to mod pedal, send sysex to change that value
  } else if (MOD_PEDAL_PARAM == number && modPedalParam > -1) {
    createAndSendSysexMessage(modPedalParamGroup, modPedalParam, value);
  } else if (SUSTAIN_PEDAL_PARAM == number && vcaHoldActive) {
    if (value < 64) {
      //If sustain off is sent, we want to retrigger the sustain to mimic hold.
      //To do this properly we would actually not want to send the sustain off message at all, but it's done automatically by the soft thru logic,
      //and I haven't found a way to filter out certain messages only.
      sendSustainMessage = true;
    }
  }
}

void setup() {
  initializePins();
  mapControlParamsToPins();
  
  analogReadAveraging(ANALOG_READ_AVERAGING);

  MIDI.setHandleNoteOn(handleMidiNoteOn);
  MIDI.setHandleNoteOff(handleMidiNoteOff);
  MIDI.setHandleControlChange(handleMidiControlChange);
  MIDI.begin(midiChannel);
  
  delay(200);
}

void loop() {
  readControlDumpButton();
  readModWheelAssignButton();
  readModPedalAssignButton();
  readVcaHoldButton();
  
  readControlInputsAndMidiIn();
  pwmLedIndicator.update();
  
  readMidiIn();
  midiLedIndicator.update();

  if (lfoDestinationParamGroup >= 0 && lfoDestinationParam >= 0 && lfoMetro.check()) { // check if the metro has passed it's interval .
    lfo.update();
    
    int lfoValue = lfo.getUpdatedValue();
    //Serial.println(lfoValue);
    createAndSendSysexMessage(lfoDestinationParamGroup, lfoDestinationParam, lfoValue);
    
    lfoMetro.reset();
  }
}

void readMidiIn() {
  MIDI.read();
}

void readControlDumpButton() {
  if (controlDumpButton.update()) {
    if (controlDumpButton.fallingEdge()) {
      forceSendControlSysex = true;
      lfoConfigModeActive = false;
    } else {
      lfoConfigModeActive = true;
    }
  } else if (forceSendControlSysex) {
    forceSendControlSysex = false;
    //Serial.println("Controldump");
  }
}

void readModWheelAssignButton() {
  if (modWheelAssignButton.update()) {
    if (modWheelAssignButton.risingEdge()) {
      //Serial.println("Mod wheel assign on");
      modWheelParam = -1;
      modWheelAssignActive = true;
    } else {
      //Serial.println("Mod wheel assign off");
      modWheelAssignActive = false;
    }
  }
}

void readModPedalAssignButton() {
  if (modPedalAssignButton.update()) {
    if (modPedalAssignButton.risingEdge()) {
      //Serial.println("Mod pedal assign on");
      modPedalParam = -1;
      modPedalAssignActive = true;
    } else {
      //Serial.println("Mod pedal assign off");
      modPedalAssignActive = false;
    }
  }
}

void readVcaHoldButton() {
  if (sendSustainMessage) {
    sendSustainChange(true);
    sendSustainMessage = false;
  }
  
  if (vcaHoldButton.update()) {
    vcaHoldActive = vcaHoldButton.fallingEdge();
//      Serial.println("VCA Hold switch change");
    //Mimic hold on/off using sustain
    sendSustainChange(vcaHoldActive);
  }
}

void readControlInputsAndMidiIn() {
  for (int controlIndex = 0; controlIndex < NUMBER_OF_CONTROLS; controlIndex++) {
    ControlPin& control = controlPins[controlIndex];

    int value = readControlInput(control);

    if (forceSendControlSysex || valueHasChanged(value, control)) {
      control.previousValue = value;
      
      if (lfoConfigModeActive) {
        if (control.paramGroup == PARAM_GROUP_TONE && control.param == TONE_PARAM_LFO_RATE) {
          lfo.setFrequencyIndex(value);
        } else {
          lfoDestinationParamGroup = control.paramGroup;
          lfoDestinationParam = control.param;
          lfo.setMaxValue(value);
        }
      } else {
        if (!forceSendControlSysex && lfo.isActive() && control.paramGroup == lfoDestinationParamGroup && control.param == lfoDestinationParam) {
          lfo.setMinValue(value);
        } else {
          //Serial.println(value);
          sendSysEx(value, control);
        }
      }
    }

    // Read midi in every 7 control pin readings to reduce latency
//    if (controlIndex % 7 == 0) {
//      readMidiIn();
//    }
  }
}

bool valueHasChanged(int value, struct ControlPin& control) {
  return value != control.previousValue;
}

int readControlInput(struct ControlPin& control) {
  selectMultiplexerChannel(control.muxChannel);
  
  if (control.maxValue > 1) {
    return readAnalogControlInput(control);
  } else if (control.maxValue == 1) {
    return readDigitalControlInput(control);
  }

  return control.previousValue;
}

void selectMultiplexerChannel(int channel) {
  static int selectedChannel = -1;

  if (channel > -1 && selectedChannel != channel) {
    digitalWrite(MULTIPLEXER_CHANNEL_CONTROL_PIN_3, HIGH && (channel & B00000100));
    digitalWrite(MULTIPLEXER_CHANNEL_CONTROL_PIN_2, HIGH && (channel & B00000010));
    digitalWrite(MULTIPLEXER_CHANNEL_CONTROL_PIN_1, HIGH && (channel & B00000001));
  
    selectedChannel = channel;
    
    // allow signals to stablize
    delayMicroseconds(MUX_CHANNEL_SWITCH_DELAY_IN_MICROSECONDS);
  }
}

int readAnalogControlInput(struct ControlPin& control) {
  int reading = analogRead(control.pin);
  
  if (forceSendControlSysex || readingHasChanged(reading, control)) {
    control.previousReading = reading;
    
    int processedValue = mapToParamValue(reading, control);
    return doParamSpecificProcessing(processedValue, control.param, control.paramGroup);
  } else {
    return control.previousValue;
  }
}

bool readingHasChanged(int reading, struct ControlPin& control) {
    return abs(reading-control.previousReading) > jitterThreshold;
}

int mapToParamValue(int reading, struct ControlPin& control) {
  //The following prevents false value changes by checking if the high resolution reading difference is large enough.
  int value = map(reading, 0, maxAnalogReadValue+1, 0, control.maxValue);
      /*
      Serial.print("Controlparam: ");
      Serial.println(control.param);
      Serial.print("Value: ");
      Serial.println(value);
      Serial.print("Reading: ");
      Serial.println(reading);
      Serial.print("Max value: ");
      Serial.println(control.maxValue);
      */
  return value;
}

int doParamSpecificProcessing(int value, int param, int paramGroup) {
  if (paramGroup == PARAM_GROUP_TONE) {
    switch (param) {
      case TONE_PARAM_BEND_RANGE:
        value++; // Useless to have 0 as possible bend range, and wanted to fit in the max range 12 in a 12 step rotary switch.
        createAndSendSysexMessage(PARAM_GROUP_PATCH, PATCH_PARAM_MONO_BEND_RANGE, value); //Mono and poly bend range are two separate params so we need to send an extra message. 
        break;
      default :
        break;
    }
  } else if (paramGroup == PARAM_GROUP_PATCH) {
    switch (param) {
      case PATCH_PARAM_TRANSPOSE:
        value -= (TRANSPOSE_MAX_VALUE >> 1); //MKS-50 wants "negative" values as MAX_VALUE - theUnsignedNegativeValue
        break;
      case PATCH_PARAM_FINE_TUNE:
        value -= (FINE_TUNE_MAX_VALUE >> 1); //MKS-50 wants "negative" values as MAX_VALUE - theUnsignedNegativeValue
        break;
      default :
        break;
    }
  }
  if (value < 0) {
    value = value + MKS50_CONTROL_MAX_VALUE + 1; //TODO Can't remember why I'm doing this. Probably not necessary.
  }
  //Serial.println();
  //Serial.println("No match");
  return value;
}

void sendSysEx(int value, struct ControlPin& control) {
  if (control.param == PATCH_PARAM_POLYPHONY_SWITCH) {
    if (value == HIGH) {
      //Monophonic performance
      //There is no "real" monophonic mode on the MKS-50,
      //so we use the chord memory and the first "chord", which by default makes all voices play one note at the same time (if I understand it correctly).
      createAndSendSysexMessage(PARAM_GROUP_PATCH, PATCH_PARAM_CHORD_MEMORY, 0);
      createAndSendSysexMessage(PARAM_GROUP_PATCH, PATCH_PARAM_POLYPHONY_SWITCH, POLYPHONY_CHORD);
    } else {
      //Polyphonic performance
      createAndSendSysexMessage(PARAM_GROUP_PATCH, PATCH_PARAM_POLYPHONY_SWITCH, POLYPHONY_POLY);
    }
    if (vcaHoldActive) {
      sendSustainMessage = true;//Sound/sustain resets when polyphony switches, we need to retrigger it
    }
  } else {
    if(modWheelAssignActive) {
    modWheelParam = control.param;
    modWheelParamGroup = control.paramGroup;
    createAndSendSysexMessage(PARAM_GROUP_PATCH, PATCH_PARAM_MOD_SENSITIVITY, 0);
    //      Serial.print("Assigning param ");
    //      Serial.print(control.param);
    //      Serial.println(" to mod wheel");
    }
      
    if(modPedalAssignActive) {
    modPedalParam = control.param;
    modPedalParamGroup = control.paramGroup;
    //      Serial.print("Assigning param ");
    //      Serial.print(control.param);
    //      Serial.println(" to mod pedal");
    }
    
    pwmLedIndicator.passValue(control.paramGroup, control.param, value);
    createAndSendSysexMessage(control.paramGroup, control.param, value); 
  }
}

int readDigitalControlInput(struct ControlPin& control) {
  return digitalRead(control.pin);
}

void createAndSendSysexMessage(uint8_t group, uint8_t param, uint8_t value) {
//  Serial.print("Controlparam: ");
//  Serial.println(param);
//  Serial.print("Value: ");
//  Serial.println(value);
  MIDI.sendSysEx(NUMBER_OF_SYSEX_DIGITS, buildSysExMessage(group, param, value), true);
}

const uint8_t* buildSysExMessage(uint8_t group, uint8_t param, uint8_t value) {
  static uint8_t data[NUMBER_OF_SYSEX_DIGITS] = {0xF0, 0x41, 0x36, 0x0, 0x23, 0x20, 0x01, 0x0, 0x0, 0xF7 };
  data[3] = midiChannel-1;
  data[5] = group;
  data[7] = param;
  data[8] = value;

  return data;
}

void sendSustainChange(bool on) {
  int value;
  if (on) {
    value = 127;
  } else {
    value = 0;
  }
  
  MIDI.sendControlChange(SUSTAIN_PEDAL_PARAM, value, midiChannel);
}

void initializePins() {
    pinMode(MULTIPLEXER_CHANNEL_CONTROL_PIN_1, OUTPUT);
    pinMode(MULTIPLEXER_CHANNEL_CONTROL_PIN_2, OUTPUT);
    pinMode(MULTIPLEXER_CHANNEL_CONTROL_PIN_3, OUTPUT);
  
    pinMode(DIGITAL_INPUT_PIN_DUMP_CONTROLS_BUTTON, INPUT_PULLUP);
    pinMode(DIGITAL_INPUT_PIN_VCA_HOLD, INPUT_PULLUP);
    pinMode(DIGITAL_INPUT_PIN_MOD_WHEEL_ASSIGN_BUTTON, INPUT_PULLUP);
    pinMode(DIGITAL_INPUT_PIN_MOD_PEDAL_ASSIGN_BUTTON, INPUT_PULLUP);
    
    pinMode(DIGITAL_INPUT_PIN_CHORUS_SWITCH, INPUT_PULLUP);
    pinMode(DIGITAL_INPUT_PIN_PORTAMENTO_SWITCH, INPUT_PULLUP);
    pinMode(DIGITAL_INPUT_PIN_POLYPHONY_SWITCH, INPUT_PULLUP);
  
    analogReadResolution(DEFAULT_ANALOG_READ_RESOLUTION);
}

//Midi controls are mapped to [multiplexerChannel][multiplexerIndex]
void mapControlParamsToPins() {
////////******** MULTIPLEXER 1 ********////////
  
  //LFO
  controlPins[0]  = {ANALOG_INPUT_PIN_1,                   0, PARAM_GROUP_TONE,  TONE_PARAM_LFO_RATE,           MKS50_CONTROL_MAX_VALUE};
  controlPins[5]  = {ANALOG_INPUT_PIN_1,                   1, PARAM_GROUP_TONE,  TONE_PARAM_LFO_DELAY,          MKS50_CONTROL_MAX_VALUE};
  
  //DCO
  controlPins[10] = {ANALOG_INPUT_PIN_1,                   2, PARAM_GROUP_PATCH, PATCH_PARAM_TRANSPOSE,         TRANSPOSE_MAX_VALUE};
  controlPins[15] = {ANALOG_INPUT_PIN_1,                   3, PARAM_GROUP_TONE,  TONE_PARAM_DCO_OCTAVE,         DCO_OCTAVE_MAX_VALUE};  
  controlPins[20] = {ANALOG_INPUT_PIN_1,                   4, PARAM_GROUP_TONE,  TONE_PARAM_DCO_NOISE_LEVEL,    DCO_NOISE_LEVEL_MAX_VALUE};  
  controlPins[25] = {ANALOG_INPUT_PIN_1,                   5, PARAM_GROUP_PATCH, PATCH_PARAM_FINE_TUNE,         FINE_TUNE_MAX_VALUE};
  controlPins[30] = {ANALOG_INPUT_PIN_1,                   6, PARAM_GROUP_TONE,  TONE_PARAM_DCO_PWM_DEPTH,      MKS50_CONTROL_MAX_VALUE};
  controlPins[35] = {ANALOG_INPUT_PIN_1,                   7, PARAM_GROUP_TONE,  TONE_PARAM_DCO_WAVEFORM_PULSE, DCO_WAVEFORM_PULSE_MAX_VALUE};
  
////////******** MULTIPLEXER 2 ********////////
  
  //DCO
  controlPins[1]  = {ANALOG_INPUT_PIN_2,                   0, PARAM_GROUP_TONE,  TONE_PARAM_DCO_PWM_RATE,       MKS50_CONTROL_MAX_VALUE};
  controlPins[6]  = {ANALOG_INPUT_PIN_2,                   1, PARAM_GROUP_TONE,  TONE_PARAM_DCO_WAVEFORM_SAW,   DCO_WAVEFORM_SAW_MAX_VALUE};
  controlPins[11] = {ANALOG_INPUT_PIN_2,                   2, PARAM_GROUP_TONE,  TONE_PARAM_DCO_SUB_LEVEL,      DCO_SUB_LEVEL_MAX_VALUE};  
  controlPins[16] = {ANALOG_INPUT_PIN_2,                   3, PARAM_GROUP_TONE,  TONE_PARAM_DCO_WAVEFORM_SUB,   DCO_WAVEFORM_SUB_MAX_VALUE};
  controlPins[21] = {ANALOG_INPUT_PIN_2,                   4, PARAM_GROUP_TONE,  TONE_PARAM_DCO_ENV_MODE,       ENV_MODE_MAX_VALUE};
  controlPins[26] = {ANALOG_INPUT_PIN_2,                   5, PARAM_GROUP_TONE,  TONE_PARAM_DCO_ENV_MOD,        MKS50_CONTROL_MAX_VALUE};
  controlPins[31] = {ANALOG_INPUT_PIN_2,                   6, PARAM_GROUP_TONE,  TONE_PARAM_DCO_AFTERTOUCH_MOD, MKS50_CONTROL_MAX_VALUE};
  controlPins[36] = {ANALOG_INPUT_PIN_2,                   7, PARAM_GROUP_TONE,  TONE_PARAM_DCO_LFO_MOD,        MKS50_CONTROL_MAX_VALUE};
  
////////******** MULTIPLEXER 3 ********////////

  //Master
  controlPins[2]  = {ANALOG_INPUT_PIN_3,                   0, PARAM_GROUP_TONE,  TONE_PARAM_BEND_RANGE,         BEND_RANGE_MAX_VALUE};
  controlPins[7]  = {ANALOG_INPUT_PIN_3,                   1, PARAM_GROUP_PATCH, PATCH_PARAM_MASTER_VOLUME,     MKS50_CONTROL_MAX_VALUE};
  
  //Glide
  controlPins[12] = {ANALOG_INPUT_PIN_3,                   2, PARAM_GROUP_PATCH, PATCH_PARAM_PORTAMENTO_TIME,   MKS50_CONTROL_MAX_VALUE};
  
  //Chorus
  controlPins[17] = {ANALOG_INPUT_PIN_3,                   3, PARAM_GROUP_TONE,  TONE_PARAM_CHORUS_RATE,        MKS50_CONTROL_MAX_VALUE};
  
  //VCA
  controlPins[22] = {ANALOG_INPUT_PIN_3,                   4, PARAM_GROUP_TONE,  TONE_PARAM_VCA_LEVEL,          MKS50_CONTROL_MAX_VALUE};
  controlPins[27] = {ANALOG_INPUT_PIN_3,                   5, PARAM_GROUP_TONE,  TONE_PARAM_VCA_ENV_MODE,       ENV_MODE_MAX_VALUE};
  controlPins[32] = {ANALOG_INPUT_PIN_3,                   6, PARAM_GROUP_TONE,  TONE_PARAM_VCA_AFTERTOUCH_MOD, MKS50_CONTROL_MAX_VALUE};
  
////////******** MULTIPLEXER 4 ********////////

  //VCF
  controlPins[3]  = {ANALOG_INPUT_PIN_4,                   0, PARAM_GROUP_TONE,  TONE_PARAM_VCF_CUTOFF,         MKS50_CONTROL_MAX_VALUE};
  controlPins[8]  = {ANALOG_INPUT_PIN_4,                   1, PARAM_GROUP_TONE,  TONE_PARAM_VCF_RESONANCE,      MKS50_CONTROL_MAX_VALUE};
  controlPins[13] = {ANALOG_INPUT_PIN_4,                   2, PARAM_GROUP_TONE,  TONE_PARAM_HPF_CUTOFF,         HPF_MAX_VALUE};
  controlPins[18] = {ANALOG_INPUT_PIN_4,                   3, PARAM_GROUP_TONE,  TONE_PARAM_VCF_KEY_FOLLOW,     MKS50_CONTROL_MAX_VALUE};
  controlPins[23] = {ANALOG_INPUT_PIN_4,                   4, PARAM_GROUP_TONE,  TONE_PARAM_VCF_ENV_MOD,        MKS50_CONTROL_MAX_VALUE};
  controlPins[28] = {ANALOG_INPUT_PIN_4,                   5, PARAM_GROUP_TONE,  TONE_PARAM_VCF_LFO_MOD,        MKS50_CONTROL_MAX_VALUE};
  controlPins[33] = {ANALOG_INPUT_PIN_4,                   6, PARAM_GROUP_TONE,  TONE_PARAM_VCF_ENV_MODE,       ENV_MODE_MAX_VALUE};
  controlPins[37] = {ANALOG_INPUT_PIN_4,                   7, PARAM_GROUP_TONE,  TONE_PARAM_VCF_AFTERTOUCH_MOD, MKS50_CONTROL_MAX_VALUE};
  
////////******** MULTIPLEXER 5 ********////////

  //Envelope
  controlPins[4]  = {ANALOG_INPUT_PIN_5,                   0, PARAM_GROUP_TONE,  TONE_PARAM_ENV_T1,             MKS50_CONTROL_MAX_VALUE};
  controlPins[9]  = {ANALOG_INPUT_PIN_5,                   1, PARAM_GROUP_TONE,  TONE_PARAM_ENV_L1,             MKS50_CONTROL_MAX_VALUE};
  controlPins[14] = {ANALOG_INPUT_PIN_5,                   2, PARAM_GROUP_TONE,  TONE_PARAM_ENV_T2,             MKS50_CONTROL_MAX_VALUE};
  controlPins[19] = {ANALOG_INPUT_PIN_5,                   3, PARAM_GROUP_TONE,  TONE_PARAM_ENV_L2,             MKS50_CONTROL_MAX_VALUE};
  controlPins[24] = {ANALOG_INPUT_PIN_5,                   4, PARAM_GROUP_TONE,  TONE_PARAM_ENV_T3,             MKS50_CONTROL_MAX_VALUE};
  controlPins[29] = {ANALOG_INPUT_PIN_5,                   5, PARAM_GROUP_TONE,  TONE_PARAM_ENV_L3,             MKS50_CONTROL_MAX_VALUE};
  controlPins[34] = {ANALOG_INPUT_PIN_5,                   6, PARAM_GROUP_TONE,  TONE_PARAM_ENV_T4,             MKS50_CONTROL_MAX_VALUE};
  controlPins[38] = {ANALOG_INPUT_PIN_5,                   7, PARAM_GROUP_TONE,  TONE_PARAM_ENV_KEY_FOLLOW,     MKS50_CONTROL_MAX_VALUE};

////////****** DIGITAL SWITCHES ******////////

  controlPins[39] = {DIGITAL_INPUT_PIN_CHORUS_SWITCH,     -1, PARAM_GROUP_TONE,  TONE_PARAM_CHORUS_SWITCH,      1};
  controlPins[40] = {DIGITAL_INPUT_PIN_PORTAMENTO_SWITCH, -1, PARAM_GROUP_PATCH, PATCH_PARAM_PORTAMENTO_SWITCH, 1};
  controlPins[41] = {DIGITAL_INPUT_PIN_POLYPHONY_SWITCH,  -1, PARAM_GROUP_PATCH, PATCH_PARAM_POLYPHONY_SWITCH,  1};
}
